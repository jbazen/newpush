var testApp = angular.module('testApp', ['720kb.datepicker', 'ngFileUpload']);

testApp.service('dataService', function($window) {
    var KEY = 'SharedData';

    var setData = function(newObj) {
        $window.sessionStorage.setItem(KEY, JSON.stringify(newObj));
    };

    var getData = function(){
        var sharedData = $window.sessionStorage.getItem(KEY);

        if (sharedData) {
            sharedData = JSON.parse(sharedData);
        }

        return sharedData;
    };

    return {
        setData: setData,
        getData: getData
    };
});

testApp.controller('postController', function($scope, $location, $window, $http, Upload, dataService) {
    $scope.todayDate = new Date();

    $http({
        method: 'GET',
        url: 'https://api.openaq.org/v1/countries'
    }).then(function (response) {
        $scope.countries = response.data.results;
    });

    $scope.uploadFile = function(file) {
        file.upload = Upload.upload({
            url: '/upload',
            data: {firstName: $scope.candidate.firstName, lastName: $scope.candidate.lastName,
                   country: $scope.candidate.country, submitDate: $scope.candidate.submitDate, file: file}
        }).then(function(response) {
            dataService.setData(response.data);
            $window.location.href = '/cv.html';
        });
    };
});

testApp.controller('cvController', function($scope, dataService) {
    $scope.candidate = dataService.getData();
});
