package com.jhb.newpush.testapp.entity;

import javax.persistence.*;
import java.util.Arrays;
import java.util.Date;
import java.util.Objects;

@Entity
public class Candidate {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String firstName;

    private String lastName;

    private String country;

    private String submitDate;

    @Lob
    private byte[] cv;

    private String cvName;

    private String cvFileType;

    private Date created;

    private Date updated;

    @PrePersist
    public void onPrePersist() {
        this.created = new Date();
    }

    @PreUpdate
    public void onPreUpdate() {
        this.updated = new Date();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getSubmitDate() {
        return submitDate;
    }

    public void setSubmitDate(String submitDate) {
        this.submitDate = submitDate;
    }

    public byte[] getCv() {
        return cv;
    }

    public void setCv(byte[] cv) {
        this.cv = cv;
    }

    public String getCvName() {
        return cvName;
    }

    public void setCvName(String cvName) {
        this.cvName = cvName;
    }

    public String getCvFileType() {
        return cvFileType;
    }

    public void setCvFileType(String cvFileType) {
        this.cvFileType = cvFileType;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getUpdated() {
        return updated;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        Candidate candidate = (Candidate) o;

        return Objects.equals(id, candidate.id) &&
                Objects.equals(firstName, candidate.firstName) &&
                Objects.equals(lastName, candidate.lastName) &&
                Objects.equals(country, candidate.country) &&
                Objects.equals(submitDate, candidate.submitDate) &&
                Arrays.equals(cv, candidate.cv) &&
                Objects.equals(cvName, candidate.cvName) &&
                Objects.equals(cvFileType, candidate.cvFileType) &&
                Objects.equals(created, candidate.created) &&
                Objects.equals(updated, candidate.updated);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(id, firstName, lastName, country, submitDate, cvName, cvFileType, created, updated);

        result = 31 * result + Arrays.hashCode(cv);

        return result;
    }

    @Override
    public String toString() {
        return "Candidate{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", country='" + country + '\'' +
                ", submitDate='" + submitDate + '\'' +
                ", cv=" + cv +
                ", cvName='" + cvName + '\'' +
                ", cvFileType='" + cvFileType + '\'' +
                ", created=" + created +
                ", updated=" + updated +
                '}';
    }

}
