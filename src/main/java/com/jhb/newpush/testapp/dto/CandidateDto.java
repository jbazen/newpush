package com.jhb.newpush.testapp.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Arrays;
import java.util.Objects;

@JsonIgnoreProperties(ignoreUnknown = true)
public class CandidateDto {

    @JsonProperty(required = false)
    private Long id;

    private String firstName;

    private String lastName;

    private String country;

    private String submitDate;

    @JsonProperty(required = false)
    private byte[] cv;

    @JsonProperty(required = false)
    private String cvName;

    @JsonProperty(required = false)
    private String cvFileType;

    public CandidateDto(String firstName, String lastName, String country, String submitDate, byte[] cv,
                        String cvName) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.country = country;
        this.submitDate = submitDate;
        this.cv = cv;
        this.cvName = cvName;
    }

    public CandidateDto() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getSubmitDate() {
        return submitDate;
    }

    public void setSubmitDate(String submitDate) {
        this.submitDate = submitDate;
    }

    public byte[] getCv() {
        return cv;
    }

    public void setCv(byte[] cv) {
        this.cv = cv;
    }

    public String getCvName() {
        return cvName;
    }

    public void setCvName(String cvName) {
        this.cvName = cvName;
    }

    public String getCvFileType() {
        return cvFileType;
    }

    public void setCvFileType(String cvFileType) {
        this.cvFileType = cvFileType;
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(id, firstName, lastName, country, submitDate, cvName, cvFileType);

        result = 31 * result + Arrays.hashCode(cv);

        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        CandidateDto that = (CandidateDto) o;

        return Objects.equals(id, that.id) &&
                Objects.equals(firstName, that.firstName) &&
                Objects.equals(lastName, that.lastName) &&
                Objects.equals(country, that.country) &&
                Objects.equals(submitDate, that.submitDate) &&
                Arrays.equals(cv, that.cv) &&
                Objects.equals(cvName, that.cvName) &&
                Objects.equals(cvFileType, that.cvFileType);
    }

    @Override
    public String toString() {
        return "CandidateDto{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", country='" + country + '\'' +
                ", submitDate='" + submitDate + '\'' +
                ", cv=" + cv +
                ", cvName='" + cvName + '\'' +
                ", cvFileType='" + cvFileType + '\'' +
                '}';
    }

}
