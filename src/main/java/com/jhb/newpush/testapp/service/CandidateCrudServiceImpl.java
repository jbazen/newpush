package com.jhb.newpush.testapp.service;

import com.jhb.newpush.testapp.dto.CandidateDto;
import com.jhb.newpush.testapp.entity.Candidate;
import com.jhb.newpush.testapp.mapper.DtoEntityMapper;
import com.jhb.newpush.testapp.repository.CandidateRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class CandidateCrudServiceImpl implements CandidateCrudService {

    private CandidateRepository candidateRepository;

    private DtoEntityMapper dtoEntityMapper;

    @Autowired
    public CandidateCrudServiceImpl(CandidateRepository candidateRepository, DtoEntityMapper dtoEntityMapper) {
        this.candidateRepository = candidateRepository;
        this.dtoEntityMapper = dtoEntityMapper;
    }

    public byte[] getCv(Long id) {
        return candidateRepository.findById(id).get().getCv();
    }

    public CandidateDto saveCandidate(CandidateDto candidateDto) {
        Candidate candidate = dtoEntityMapper.map(candidateDto, Candidate.class);
        candidate = candidateRepository.save(candidate);

        return dtoEntityMapper.map(candidate, CandidateDto.class);
    }

}
