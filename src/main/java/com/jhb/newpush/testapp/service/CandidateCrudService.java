package com.jhb.newpush.testapp.service;

import com.jhb.newpush.testapp.dto.CandidateDto;

public interface CandidateCrudService {

    public byte[] getCv(Long id);

    public CandidateDto saveCandidate(CandidateDto candidateDto);

}
