package com.jhb.newpush.testapp.controller;

import com.jhb.newpush.testapp.dto.CandidateDto;
import com.jhb.newpush.testapp.service.CandidateCrudService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@RestController
public class CandidateController {

    private CandidateCrudService candidateCrudService;

    @Autowired
    public CandidateController(CandidateCrudService candidateCrudService) {
        this.candidateCrudService = candidateCrudService;
    }

    @RequestMapping(path = "/api/v1/files/{id}", method = RequestMethod.GET)
    public byte[] getFile(@PathVariable Long id) {
        return candidateCrudService.getCv(id);
    }

    @RequestMapping(path = "/upload", method = RequestMethod.POST)
    public CandidateDto upload(@RequestParam(value = "firstName") String firstName,
                               @RequestParam(value = "lastName") String lastName,
                               @RequestParam(value = "country") String country,
                               @RequestParam(value = "submitDate") String submitDate,
                               @RequestParam(value = "file") MultipartFile file) throws IOException {
        CandidateDto candidate = new CandidateDto(firstName, lastName, country, submitDate, file.getBytes(),
                file.getOriginalFilename());
        candidate = candidateCrudService.saveCandidate(candidate);

        return candidate;
    }

}
