package com.jhb.newpush.testapp.repository;

import com.jhb.newpush.testapp.entity.Candidate;
import org.springframework.data.repository.CrudRepository;

public interface CandidateRepository extends CrudRepository<Candidate, Long> {

}
