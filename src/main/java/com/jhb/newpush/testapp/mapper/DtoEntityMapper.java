package com.jhb.newpush.testapp.mapper;

import com.jhb.newpush.testapp.dto.CandidateDto;
import com.jhb.newpush.testapp.entity.Candidate;
import ma.glasnost.orika.CustomMapper;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.MappingContext;
import ma.glasnost.orika.impl.ConfigurableMapper;
import org.springframework.stereotype.Component;

@Component
public class DtoEntityMapper extends ConfigurableMapper {

    protected void configure(MapperFactory factory) {
        factory.classMap(CandidateDto.class, Candidate.class)
                .byDefault()
                .customize(new CustomMapper<CandidateDto, Candidate>() {

                    @Override
                    public void mapAtoB(CandidateDto a, Candidate b, MappingContext mappingContext) {
                        String[] parts = a.getCvName().split(".");

                        b.setCvFileType(parts[1]);
                    }

                })
                .register();

        factory.classMap(Candidate.class, CandidateDto.class)
                .byDefault()
                .register();
    }

}
